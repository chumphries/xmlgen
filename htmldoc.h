/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of XMLGen.
*
* XMLGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* XMLGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with XMLGen.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file htmldoc.h
  \author Colin Humphries
  \brief Classes for creating HTML documents.
*/
#ifndef _HTMLDOC_H
#define _HTMLDOC_H

#include "xmldoc.h"
#include "cssdoc.h"

namespace xmlgen {
  
  /*! \class HTMLTag
    \brief HTML tag class
    
    This class defines the base tag for an HTML document.
    
    \author Colin Humphries
  */
  class HTMLTag : public XTag {
  public:
    /*!
      Class constructor.
    */
    HTMLTag() {
      name = "html";
      phead = addElement("head");
      pbody = addElement("body");
    }
    /*!
      Set the document title
      \param[in] title string
      \param[out] pointer to the newly created element
    */
    XTag* addTitle(const std::string &title) {
      SXTag *ptag = new SXTag("title");
      ptag->addString(title);
      phead->addElement(ptag);
      return (XTag *)ptag;
    }
  protected:
    XTag *phead;
    XTag *pbody;
  };
  
  /*! \class HTMLTable
    \brief HTML table class
    
    This is a class for generating an HTML table.
    
    \author Colin Humphries
  */
  class HTMLTable : public XTag {
  public:
    /*!
      Class constructor.
    */
    HTMLTable(size_t nr, size_t nc, bool usehead) {
      name = "table";
      XTag *prow;
      for (uint ii=0; ii<nr; ++ii) {
	if (ii == 0 && usehead) {
	  prow = addElement("th");
	}
	else {
	  prow = addElement("tr");
	}
	std::vector<XTag *> xvec;
	for (uint jj=0; jj<nc; ++jj) {
	  xvec.push_back(prow->addShortElement("td"));
	}
	ptdata.push_back(xvec);
      }
    }
    XTag* Element(size_t nr, size_t nc) {return ptdata[nr][nc];}
    XTag* Cell(size_t nr, size_t nc) {return ptdata[nr][nc];}
    
  protected:
    std::vector<std::vector<XTag *> > ptdata;
  };
  
}

#endif
