/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of XMLGen.
*
* XMLGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* XMLGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with XMLGen.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file svgdoc.h
  \author Colin Humphries
  \brief Classes for creating SVG documents.
*/
#ifndef _SVGDOC_H
#define _SVGDOC_H

#include "xmldoc.h"
#include "cssdoc.h"

namespace xmlgen {
  
  /*! \class SVGTag
    \brief SVG tag class
    
    This class defines the base tag for an SVG document.
    
    \author Colin Humphries
  */
  class SVGTag : public XTag {
  public:
    /*!
      Class constructor.
    */
    SVGTag() {
      name = "svg";
      pdefs = addElement("defs");
      pg = addElement("g");
      addAttribute("xmlns","http://www.w3.org/2000/svg");
      addAttribute("xmlns:xlink","http://www.w3.org/1999/xlink");
    }
    /*!
      Output string rendering of tag and children.
    */ 
    virtual std::string Render() {return XTag::Render();}
    /*!
      Set the document width
      \param[in] w width in pixels
    */
    void setWidth(int w) {
      width = w;
      addAttribute("width",std::to_string(w));
    }
    /*!
      Set the document height
      \param[in] h height in pixels
    */
    void setHeight(int h) {
      height = h;
      addAttribute("height",std::to_string(h));
    }
    /*!
      Set the document width and height
      \param[in] w width in pixels
      \param[in] h height in pixels
    */
    void setSize(int w, int h) {
      setWidth(w);
      setHeight(h);
    }
    /*!
      Set the document x position
      \param[in] x position
    */
    void setX(double x) {
      addAttribute("x",std::to_string(x));
    }
    /*!
      Set the document y position
      \param[in] y position
    */
    void setY(double y) {
      addAttribute("y",std::to_string(y));
    }
    /*!
      Set the document x and y position
      \param[in] x position
      \param[in] y position
    */
    void setPos(double x, double y) {
      addAttribute("x",std::to_string(x));
      addAttribute("y",std::to_string(y));
    }
    /*!
      Set the document View Box
      \param[in] x position
      \param[in] y position
      \param[in] w width
      \param[in] h height
    */
    void setViewBox(double x, double y, double w, double h) {
      std::ostringstream oss;
      oss << x << " " << y << " " << w << " " << h;
      addAttribute("viewBox",oss.str());
    }
    /*!
      Add a line element to the document
      \param[in] x1 start x position
      \param[in] y1 start y position
      \param[in] x2 end x position
      \param[in] y2 end y position
      \param[out] pointer to the newly created element
    */
    XTag* addLine(double x1, double y1, double x2, double y2) {
      EXTag *ptag = new EXTag("line");
      ptag->addAttribute("x1",std::to_string(x1));
      ptag->addAttribute("y1",std::to_string(y1));
      ptag->addAttribute("x2",std::to_string(x2));
      ptag->addAttribute("y2",std::to_string(y2));
      pg->addElement(ptag);
      return (XTag *)ptag;
    }
    /*!
      Add a poly line element to the document
      \param[in] xdata vector of x positions
      \param[in] ydata vector of y positions
      \param[out] pointer to the newly created element
    */
    XTag* addPolyLine(const std::vector<double> &xdata, const std::vector<double> &ydata) {
      EXTag *ptag = new EXTag("polyline");
      std::ostringstream spoints;
      uint N = xdata.size();
      if (ydata.size() < N) {
	N = ydata.size();
      }
      for (uint ii=0; ii<N; ++ii) {
	spoints << xdata[ii] << "," << ydata[ii];
	if (ii < (N-1)) {
	  spoints << " ";
	}
      }
      ptag->addAttribute("points",spoints.str());
      pg->addElement(ptag);
      return (XTag *)ptag;
    }
    /*!
      Add a polygon element to the document
      \param[in] xdata vector of x positions
      \param[in] ydata vector of y positions
      \param[out] pointer to the newly created element
    */
    XTag* addPolygon(const std::vector<double> &xdata, const std::vector<double> &ydata) {
      EXTag *ptag = new EXTag("polygon");
      std::ostringstream spoints;
      uint N = xdata.size();
      if (ydata.size() < N) {
	N = ydata.size();
      }
      for (uint ii=0; ii<N; ++ii) {
	spoints << xdata[ii] << "," << ydata[ii];
	if (ii < (N-1)) {
	  spoints << " ";
	}
      }
      ptag->addAttribute("points",spoints.str());
      pg->addElement(ptag);
      return (XTag *)ptag;
    }
    /*!
      Add a rectagle element to the document
      \param[in] x x-position
      \param[in] y y-position
      \param[in] w width
      \param[in] h height
      \param[out] pointer to the newly created element
    */
    XTag* addRect(double x, double y, double w, double h) {
      EXTag *ptag = new EXTag("rect");
      ptag->addAttribute("x",std::to_string(x));
      ptag->addAttribute("y",std::to_string(y));
      ptag->addAttribute("width",std::to_string(w));
      ptag->addAttribute("height",std::to_string(h));
      pg->addElement(ptag);
      return (XTag *)ptag;
    }
    XTag* addText(double x1, double y1, const std::string &tstr) {
      SXTag *ptag = new SXTag("text");
      ptag->addAttribute("x",std::to_string(x1));
      ptag->addAttribute("y",std::to_string(y1));
      ptag->addString(tstr);
      pg->addElement(ptag);
      return (XTag *)ptag;
    }
    XTag* addCircle(double x1, double y1, double radius) {
      EXTag *ptag = new EXTag("circle");
      ptag->addAttribute("cx",std::to_string(x1));
      ptag->addAttribute("cy",std::to_string(y1));
      ptag->addAttribute("r",std::to_string(radius));
      pg->addElement(ptag);
      return (XTag *)ptag;
    }
    XTag* addG() {
      XTag *ptag = new XTag("g");
      pg->addElement(ptag);
      return ptag;
    }
    void addGElement(XTag *pxt) {
      pg->addElement(pxt);
    }
    void insertGraphics(XTag *pxt) {
      pg->addElement(pxt);
    }
    
  protected:
    XTag *pdefs;
    XTag *pg;
    int width;
    int height;
    
  };
  
  std::string CSSColor(double r, double g, double b) {
    int red = (int)floor(r*255.0);
    int green = (int)floor(g*255.0);
    int blue = (int)floor(b*255.0);
    std::ostringstream cstr;
    cstr << "rgb(" << red << "," << green << "," << blue << ")";
    return cstr.str();
  }
  
  std::string CSSColor(double r, double g, double b, double a) {
    int red = (int)floor(r*255.0);
    int green = (int)floor(g*255.0);
    int blue = (int)floor(b*255.0);
    std::ostringstream cstr;
    cstr << "rgba(" << red << "," << green << "," << blue << "," << a << ")";
    return cstr.str();
  }
  std::string HSLColor(int h, int s, int l) {
    // h is 0 to 360
    // s,l are 0 to 100
    std::ostringstream cstr;
    cstr << "hsl(" << h << "," << s << "%," << l << "%)";
    return cstr.str();
  }
  
}

#endif
