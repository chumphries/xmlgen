/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of XMLGen.
*
* XMLGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* XMLGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with XMLGen.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file xmldoc.h
  \author Colin Humphries
  \brief Classes for creating XML-like documents.
*/
#ifndef _XMLDOC_H
#define _XMLDOC_H

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
// #include <cmath>

// using namespace std;

namespace xmlgen {
  
  enum Tag_Display_t {FULL, SHORT, EMPTY, HEMPTY, BLANK};
  
  /*! \class XObject
    \brief Base XML object class
    
    This is a generic class that is the base for XML/SVG/HTML doc classes.
    
    The class defines a virtual Render() method for outputting text. 
    
    The class also keeps track of any modifications which might require re-rendering.
    \author Colin Humphries
  */
  class XObject {
  public:
    /*!
      Class constructor.
    */
    XObject() {ismodified = true;}
    virtual ~XObject() {;}
    /*!
      Virtual render function
    */  
    virtual std::string Render() = 0;
  protected:
    bool ismodified;
  };
  /*! \class XString
    \brief XML string class
    
    This class renders a simple string of text without any tags.
    
    \author Colin Humphries
  */
  class XString : public XObject {
  public:
    /*!
      Class constructor.
    */  
    XString() {;}
    /*!
      Class constructor.
      \param[in] str string text
    */
    XString(const std::string &str) {text = str; ismodified = true;}
    /*!
      Output text string
    */   
    std::string Render() {
      ismodified = false;
      return text;
    }
    /*!
      Set text string
      \param[in] str new text string
    */  
    void setString(const std::string &str) {text = str;}
  private:
    std::string text;
  };
  /*! \class XTag
    \brief Basic XML tag class
    
    This class defines an XML tag, which includes the tag name, attributes, and children.
    
    For example: <div id="main">Hello<i>there</i></div>
    
    The name of the tag is 'div'. It has one attribute called 'id' whose
    value is 'main'. It has two children. The first is a string of text
    'Hello' defined in an XString class. The second is another XTag with
    the tag name of 'i'.  
    
    The display of the tag can be set to several options.
    
    FULL : Opening and closing tags. All children are rendered on new lines.
     <tag>
     <subtag>
     </tag>
    SHORT : Opening and clossing tags. All children are rendered on the same line.
     <tag><subtag></tag>
    EMPTY : No closing tag. This tag type does not have children.
     <tag />
    HEMPTY : No closing tag. This tag type does not have children. The tag is rendered in HTML style. e.g. <br>
     <tag>
    BLANK : The tag and children are not displayed/rendered.
  
    \author Colin Humphries
  */
  class XTag : public XObject {
  public:
    /*!
      Class constructor.
    */
    XTag() {display = FULL;}
    /*!
      Class constructor.
      /param[in] tname tag name
    */  
    XTag(const std::string &tname) {
      display = FULL;
      name = tname;
    }
    /*!
      Class destructor.
    */  
    ~XTag() {
      for (std::vector<XObject *>::iterator it = children.begin();
	   it != children.end(); ++it) {
	delete *it;
      }
    }
    /*!
      Output string rendering of tag and children.
    */ 
    virtual std::string Render() {
      std::ostringstream ss;
      std::string stmp;
      if (display == BLANK) {
	
      }
      else {
	ss << "<" << name;
	for (std::map<std::string,std::string>::iterator it = attributes.begin();
	     it != attributes.end(); ++it) {
	  ss << " " << it->first << "=\"" << it->second << "\"";
	}
	if (display == EMPTY) {
	  ss << " />";
	}
	else if (display == HEMPTY) {
	  ss << ">";
	}
	else if (display == SHORT) {
	  ss << ">";
	  for (std::vector<XObject *>::iterator it = children.begin();
	       it != children.end(); ++it) {
	    ss << (*it)->Render();
	  }
	  ss << "</" << name << ">";
	}
	else {
	  ss << ">" << std::endl;
	  for (std::vector<XObject *>::iterator it = children.begin();
	       it != children.end(); ++it) {
	    stmp = (*it)->Render();
	    if (stmp[stmp.size()-1] == '\n') {
	      ss << stmp;
	    }
	    else {
	      ss << stmp << "\n";
	    }		
	    // ss << (*it)->Render() << "\n";
	  }
	  ss << "</" << name << ">";
	}
      }
      ismodified = false;
      return ss.str();
    }
    
    std::string Name() {return name;}
    
    /*!
      Add an attribute to the tag.
      \param[in] aname attribute name
      \param[in] avalue attribute value
    */ 
    void addAttribute(const std::string &aname, const std::string &avalue) {
      attributes[aname] = avalue;
      ismodified = true;
    }
    /*!
      Create and add a new XTag as a child of the current tag.
      \param[in] ename child tag name
      \param[out] pointer to the newly created tag
    */
    XTag* addElement(const std::string &ename) {
      XTag *pxt = new XTag(ename);
      children.push_back(pxt);
      ismodified = true;
      return pxt;
    }
    /*!
      Add an existing XObject as a child of the current tag.
      \param[in] eobj XObject pointer
    */  
    void addElement(XObject *eobj) {children.push_back(eobj);}
    /*!
      Create and add a new XTag as a child of the current tag with the display variable set to SHORT.
      \param[in] ename child tag name
      \param[out] pointer to the newly created tag
    */
    void addElement(std::vector<XObject *> &xvec) {
      for (std::vector<XObject *>::iterator it = xvec.begin();
	   it != xvec.end(); ++it) {
	children.push_back(*it);
      }
    }
    XTag* addShortElement(const std::string &ename) {
      XTag *pxt = new XTag(ename);
      pxt->setDisplay(SHORT);
      children.push_back(pxt);
      ismodified = true;
      return pxt;
    }
    /*!
      Create and add a new XTag as a child of the current tag with the display variable set to EMPTY.
      \param[in] ename child tag name
      \param[out] pointer to the newly created tag
    */  
    XTag* addEmptyElement(const std::string &ename) {
      XTag *pxt = new XTag(ename);
      pxt->setDisplay(EMPTY);
      children.push_back(pxt);
      ismodified = true;
      return pxt;
    }
    /*!
      Create and add a new XTag as a child of the current tag with the display variable set to HEMPTY.
      \param[in] ename child tag name
      \param[out] pointer to the newly created tag
    */ 
    XTag* addHEmptyElement(const std::string &ename) {
      XTag *pxt = new XTag(ename);
      pxt->setDisplay(HEMPTY);
      children.push_back(pxt);
      ismodified = true;
      return pxt;
    }
    /*!
      Create and add a new XString as a child of the current tag.
      \param[in] str child XString text
      \param[out] pointer to the newly created string tag
    */  
    XString* addString(const std::string &str) {
      XString *pxstr = new XString(str);
      children.push_back(pxstr);
      ismodified = true;
      return pxstr;
    }
    /*!
      Set the display type of the current tag.
      \param[in] dis display type
    */   
    void setDisplay(Tag_Display_t dis) {display = dis;}
    
  protected:
    std::string name;
    Tag_Display_t display;
    std::map<std::string,std::string> attributes;
    std::vector<XObject *> children;
  };
  
  /*! \class EXTag
    \brief Convience class for creating an XTag with an EMPTY display
    
    \author Colin Humphries
  */
  class EXTag : public XTag {
  public:
    EXTag() {display = EMPTY;}
    EXTag(const std::string &tname) : XTag(tname) {display = EMPTY;}
  };
  
  /*! \class SXTag
    \brief Convience class for creating an XTag with an SHORT display
    
    \author Colin Humphries
  */
  class SXTag : public XTag {
  public:
    SXTag() {display = SHORT;}
    SXTag(const std::string &tname) : XTag(tname) {display = SHORT;}
  };
  
}

#endif
  
